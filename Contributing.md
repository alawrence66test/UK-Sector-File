Welcome to the UK Sector File repository, for everything sector related in the UK!

The aim of the repository is to centralise and track changes made, and to enable easier editing of individual parts of the sector file, instead of trawling through lines and lines of data.

With developer access, you should be able to commit to the development branch, and be able to create new branches.
For bug fixes and minor enhancements, commit directly to the development branch.
For new features, such as an SMR for an airfield that has not previously had one, create a new branch with a meaningful name (e.g. egnc-smr), and then commit your new data there.

Merge requests should be created for ALL merges so we are able to keep track of when they occur, and allow others to review data as necessary.
If you are not sure how, or you do not want to create a merge request for a new branch, expect one to be created by someone at a later date!

For any questions regarding contributing to the repository, please use the helpdesk - the reason being is that more than one person has access to the EuroScope helpdesk department, so we can hopefully respond to you quicker.
Two things to bear in mind...
1. If you're not sure whether something is correct and are questioning whether you should commit it, then get a second opinion to be absolutely sure. But...
2. We can always revert changes which is the beauty of hosting the sector in a repository! Never let a mistake you've made previously hold you back from doing more, but do correct any mistakes you have made at the first opportunity.

Finally, thanks for being part of this project!